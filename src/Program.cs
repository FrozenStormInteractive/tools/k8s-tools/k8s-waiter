﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using CommandLine;
using k8s;
using k8s.Models;
using Microsoft.Extensions.Logging;

namespace K8sWaiter
{
    internal class Program
    {
        private class Options
        {
            [Value(0, Required = true, HelpText = "Job name.")]
            public required string Name { get; set; }

            [Option('n', "namespace", HelpText = "If present, the namespace scope for the resource.")]
            public string? Namespace { get; set; }

            [Option("failed-as-success", HelpText = "Treat failed status as success.", Default = false)]
            public bool FailedAsSuccess { get; set; }

            [Option('v', "verbose", Default = false)]
            public bool Verbose { get; set; }
        }

        private static async Task<int> Main(string[] args)
        {
            return await Parser.Default.ParseArguments<Options>(args).MapResult(Run, _ => Task.FromResult(1));
        }

        private static async Task<int> Run(Options options)
        {
            using var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter("Microsoft", options.Verbose ? LogLevel.Information : LogLevel.Error)
                    .AddFilter("System", options.Verbose ? LogLevel.Information : LogLevel.Error)
                    .AddFilter("K8sWaiter.Program", options.Verbose ? LogLevel.Trace : LogLevel.Information)
                    .AddConsole();
            });
            ILogger logger = loggerFactory.CreateLogger<Program>();

            try
            {
                var config = KubernetesClientConfiguration.BuildDefaultConfig();
                var client = new Kubernetes(config);

                var jobName = options.Name.Trim();
                var @namespace = options.Namespace?.Trim();

                bool? success = null;

                k8s.Autorest.HttpOperationResponse<V1JobList> jobTask;
                if (@namespace is null)
                {
                    jobTask = await client.BatchV1.ListJobForAllNamespacesWithHttpMessagesAsync(fieldSelector: $"metadata.name={jobName}", watch: true);
                }
                else
                {
                    jobTask = await client.BatchV1.ListNamespacedJobWithHttpMessagesAsync(@namespace, fieldSelector: $"metadata.name={jobName}", watch: true);
                }

                string @namespaceStr = @namespace ?? "";

                jobTask.Watch<V1Job, V1JobList>(
                    (type, item) =>
                    {
                        if (item.Status.Active.HasValue)
                        {
                            logger.LogInformation("Job {Namespace}:{ResourceName} is active...", @namespaceStr, jobName);
                        }
                        else if (item.Status.Succeeded.HasValue)
                        {
                            success = true;
                        }
                        else if (item.Status.Failed.HasValue)
                        {
                            if (options.FailedAsSuccess)
                            {
                                success = true;
                            }
                            else
                            {
                                success = false;
                            }
                        }
                    });

                logger.LogInformation("Waiting for job {Namespace}:{ResourceName}...", @namespaceStr, jobName);

                while (!success.HasValue)
                {
                }

                if (success.Value)
                {
                    logger.LogInformation("Job {Namespace}:{ResourceName} successfully completed.", @namespaceStr, jobName);
                    return 0;
                }
                else
                {
                    logger.LogInformation("Job {Namespace}:{ResourceName} ended with an error.", @namespaceStr, jobName);
                    return 1;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return 1;
            }
        }
    }
}
