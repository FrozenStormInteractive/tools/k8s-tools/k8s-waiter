FROM mcr.microsoft.com/dotnet/sdk:8.0-alpine AS build
WORKDIR /build
COPY "K8sWaiter.csproj" "K8sWaiter.csproj"
COPY "src/" "src/"
RUN dotnet publish -c Release -o /app

FROM mcr.microsoft.com/dotnet/runtime:8.0-alpine AS final
WORKDIR /app
COPY --from=build /app .
ENTRYPOINT ["dotnet", "K8sWaiter.dll"]
